const API_KEY = "METTRE VOTRE CLEF ICI";
const URL_API = "https://api.openweathermap.org/data/2.5/weather";
const DIV_WEATHER = document.querySelector(".weather");

// La méthode get de Axios renvoie une promesse
// Une promesse a trois états 
    // then -> Quand la promesse est tenue (réussite)(quand la requête est faite)
    // catch -> Quand la promesse est non tenue (erreur)
    // finally -> Quand la promesse est terminée, qu'elle ai été tenue ou non
// axios.get(`${URL_API}?q=Bruxelles&lang=fr&units=metric&appid=${API_KEY}`)
//     .then((resultat) => { console.log(resultat.data) })
//     .catch((error) => { console.log(error)})
//     .finally(() => { console.log("J'ai fini la requête");})

/** 
 * Fonction qui recherche la météo d'une ville donnée en paramètre
 * @param {string} cityName
 */
async function getWeather(cityName) {
    //Essaie de faire ma promesse (requête)
    try {
        let resultat = await axios.get(`${URL_API}?q=${cityName}&lang=fr&units=metric&appid=${API_KEY}`);
        //Await attend que ça finisse pour passer à la suite, si erreur, on est directement envoyés au block catch
        console.log(resultat.data);
        let currentWeather = resultat.data;

        DIV_WEATHER.innerHTML = `
                                    <p> Il fait actuellement ${currentWeather.main.temp}° </p>
                                    <p> Le minimun est de ${currentWeather.main.temp_min}° et le maximum est de ${currentWeather.main.temp_max}°</p>
                                    <div>
                                        <img src="https://openweathermap.org/img/wn/${currentWeather.weather[0].icon}@2x.png" alt="${currentWeather.weather[0].main}">
                                        <p>Le temps est : ${currentWeather.weather[0].description}</p>
                                    </div>
                                `
    }
    catch(error){
        console.log(error);
        document.location.href = "./notfound.html";
        DIV_WEATHER.innerHTML = `Erreur lors de la récupération de la météo !`;
    }

    console.log("J'ai fini la requête");
}