const POLAROIDS = document.querySelector(".wrapper");

// Deux méthodes pour parcourir le tableau :
// VILLES.forEach( ville => POLAROIDS.appendChild(createCard(ville.name, ville.desc, ville.url)));

for(const VILLE of VILLES) {
    POLAROIDS.appendChild(createCard(VILLE.name, VILLE.desc, VILLE.url));
}

/** 
 * Fonction qui créé un polaroid à partir des informations d'une ville
 * @param {string} name
 * @param {string} desc
 * @param {string} url
 * @returns {HTMLDivElement}
 */
function createCard(name, desc, url) {
    // Version longue mais plus clean
    // Création de la Card
    const CARD = document.createElement("div");
    CARD.classList.add("polaroids");

    const INNER_CARD = document.createElement("div");

    // Création de tous les éléments dans la card
    const TITLE = document.createElement("h2");
    const IMG = document.createElement("img");
    const DESC = document.createElement("p");
    
    // On setup toutes les infos d'une ville dans les bons éléments
    TITLE.textContent = name;
    TITLE.classList.add("cityname");
    IMG.src = "./images/" + url;
    IMG.alt = name;
    DESC.textContent = desc;

    // On ajoute les éléments à notre CARD
    INNER_CARD.append(IMG, TITLE, DESC); 
    CARD.append(INNER_CARD);
    //append : Ajoute tous les enfants


    // Rajouter le fait que chaque polaroid ouvre la modal
    CARD.addEventListener("click", () => {
        //On ouvre la modal
        MODAL.classList.remove("hidden");
        //On remplace la ville de la modal par la ville de notre card
        TITRE_MODAL.textContent = name;
        DESC_MODAL.textContent = desc;


        //On fait l'appel API pour charger la météo
        getWeather(name);
        
    });

    //On renvoie la card créée
    return CARD;

    // Version courte
    const CARD2 = document.createElement("div");
    CARD2.classList.add("polaroids");
    CARD2.innerHTML = ` <div>
                            <img src="./images/${url}" alt="${name}" />
                            <h2 class="cityname">${name}</h2>
                            <p>${desc}</p>
                        </div>`
    return CARD2;
}
/** 
 * Fonction qui créé un polaroid à partir des informations d'une ville
 * @param { { name : string, desc : string, url : string } } ville
 * @returns {HTMLDivElement}
 */
function createCard2(ville) {
    //Fait pareil mais avec l'objet ville entier
}